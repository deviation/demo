#!/usr/bin/env bash

# exec &> /dev/null

$sudo pkill tcpdump || true
[ -f "$pcap" ] && $sudo rm $pcap
[ -f "$stderr" ] && rm $stderr
[ -f "$stdout" ] && rm $stdout

$sudo tcpdump -vv -s 0 -i "$iface" -n -w $pcap &> /dev/null &

echo "Starting at:" > $log
date >> $log
