#!/usr/bin/env bash

# exec &> /dev/null

source "/$shared/sendreq.sh"

tryconn() {
    local connecttime
    local connectdate

    connecttime=$(date +%s)
    connectdate=$(date)
    # ls -la /proc/$$/fd
    echo "0 0" > $trlog
    echo "Connecting at:" >> "$log"
    echo "$connectdate" >> "$log"

    for i in $(seq 0 $imax); do
        for j in $(seq 1 $jmax); do
            date=$(date)
            time=$(($(date +%s)-$connecttime))
            sendreq $tacc conn$(($i*$jmax+$j)) successaction \
                failaction &
            sleep $interval
        done
    done
}
