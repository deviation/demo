# $shared must be set appropriately before sourcing this script

democonf="$shared/democonf.sh"
runconf="$shared/runconf.sh"
source $democonf
if [[ -f $runconf ]]; then
    source $runconf
fi

main="$( cd "$(dirname "$0")" ; pwd -P )"
mrefopt="m${software}ref"
mopt="m$software"
lopt="load$software"
m=${!mopt:-!mrefopt}
load=${!lopt}
# PS4='+$LINENO: '
PS4='+(${BASH_SOURCE}:${LINENO}): \
    ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'

traperr() {
    echo "ERROR: ${BASH_SOURCE[1]} at about \
${BASH_LINENO[0]}" >&2
}

yell() { echo "$0: $*" >&2; }
die() { yell "$*"; exit 111; }
try() { "$@" || die "cannot $*"; }

convertsecs() {
    h=$((${1}/3600))
    m=$(((${1}%3600)/60))
    s=$((${1}%60))
    printf "%02d:%02d:%02d\n" $h $m $s
}

configure() {
    local key=${1:?}
    local value=${2:?}
    local file=${3:?}
    ${sudo:=}
    if grep -qse "^$key=" $file; then
        $sudo sed -i "s/^$key=.*/$key=$value/" $file
    else
        echo "$key=$value" | $sudo tee -a $file > /dev/null
    fi
    return 0
}

trap traperr ERR
# To see the logs either:
# -   Use "journalctl SYSLOG_IDENTIFIER=./control"
# -   Use "less /var/log/syslog"
if [[ "$sourcedebug" != true ]]; then
    # Send debugging to journald
    exec 5> >(logger -t $0)
    BASH_XTRACEFD="5"
fi
if [[ "$strictmode" = true ]]; then
    set -euo pipefail
fi
set -xE
# set -v
# shopt -s extdebug
