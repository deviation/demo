# The Deviation Attack: demonstration

## Setup

I tested the demonstration on arch linux with the following programs installed:
-   VirtualBox 6.0.10
-   Vagrant 2.2.5
-   gnuplot 5.2 patchlevel 7
-   Python 3.7.3
-   GNU bash, version 5.0.7
-   bc 1.07.1

The demonstration should also work on any Linux distribution that uses systemd
and where the above packages are installed, possibly with different versions.

However, **bash must be at least version 5**, since I use some constructs
specific to bash 5.

Ensure you have an Internet connection. The virtual machines (VM) download some
packages during setup.

Then edit shared/democonf.sh (or keep defaults) and run:

    ./control setup

This takes ~15 min on a fresh install of my arch linux system. It takes 7 min
the next times you do it (if you do it again).

Check the VMs' status with: `vagrant global-status`

## Basic usage

Ensure you have followed the setup instructions above.

Copy default configuration:

    cp shared/gentableconf.sh{.defaults,}

Edit shared/gentableconf.sh (or keep defaults).

Then perform a full analysis by running:

    ./control gentable

To close the graphs:

    pkill gnuplot

When the attack is over, the results are available in results/tableavg.tex. This
is a LaTeX file. To display the results you can e.g. install latexmk and run:

    cd results
    latexmk -pdf tableavg.tex

Then open tableavg.pdf.

## Explanation

We explain here after what a full analysis does. However, note that by default
the warm-up run is disabled, there is only one cycle and one run, and the
measure of m is disabled. Edit shared/gentable.conf to change these settings.

In a full analysis we perform:
-   one warm-up run
-   mutiple cycles

In each cycle we:
-   perform runs of all ids in the `runids` configuration variable
-   report the results in results/table$id.tex

In each run we:
-   check in measuresofm.sh if we already have a measure of m for this run
-   If no measure of m is available then we perform a measure by setting up a
    bunch of connections in Victim. Then we remove all connections
-   run the Deviation Attack experiment

Each run is configured by the following variables:
-   cid: cycle id
-   id: run id
-   c : Memory capacity of Victim (in MB)
-   cs: Configured sigma

The following values will then be computed or measured:
-   m : Measured m (in kB)
-   ms: Measured sigma
-   et: Expected Tmem
-   mt: Measured Tmem

sigma is the deviation rate (i.e. the number of m1 messages deviated by the
intruder per second).

Tmem is the time in seconds at which memory exhaustion starts. Origin of time is
set at the first deviation.

m is the amount of memory occupied by one single connection in Victim.

Note that enabling the measure of m yields a better Tmem prediction.

## Tips

If you encounter an error during experiment, use the following to stop packet
capture and IKEv2 daemons. No need to run this if the experiment exits normally.

    ./control stopexp

To halt the machines, use:

    ./control halt

To boot them back:

    ./control boot

If you encounter an error while running the control script, check your logs
using:

    journalctl -nall -e

If the error happened inside a VM, use the above command inside the VM.

To connect to VM, e.g. Victim:

    cd victim
    vagrant ssh

To destroy all VMs:

    ./control destroy

Zsh completion for the `control` is available. To use it, copy the `_control`
file to a directory in your $fpath.

## Notes on our strongSwan configuration

We decided, for practical reasons, not to create the N Initiator machines of the
generic scenario, but instead to only create `N_{demo}` Initiator VMs, with each
of them sending `N/N_{demo}` m1 messages to Responder machines. However, by
default in strongSwan, a party requires an IKE ID to be unique among the IKE SAs
it manages. That is, when that party receives an IKE\_AUTH request with an IDi
payload that is equal to the peer ID of an existing IKE SA in its SAD, it will
delete the old IKE SA and replace it with the new one. Because of this in our
experiment, the Deviation Attack would add only `N_{demo}` unintended
connections in Victim. To stay faithful to the generic scenario, we tell Victim
not to delete the old IKE SA in this situation, and instead, to set up the new
one alongside. This way, the Deviation Attack adds up to `N` unintended
connections in Victim. We achieve this by setting the following option in
Victim.

    uniqueids = never


We make the Initiator VMs intending to talk to only one Responder peer. This
makes it easier to implement Intruder because that way Intruder only needs to
spoof one IP address. However in strongSwan, when a party needs to set up a new
Child SA with a peer, and already has an IKE SA set up with it, the party will
reuse this IKE SA and send a CREATE\_CHILD\_SA request. This would not be
faithful to the generic scenario. We thus make Initiator send an IKE\_SA\_INIT
request every time it wants to set up a Child SA. We achieve this by setting the
following option in the Initiator VMs.

    reuse_ikesa = no

For the purpose of detecting an eventual Denial-of-Service, we make Probe
regularly send m1 messages to Victim. However, we do not want Probe to affect
Victim's memory occupation. We thus make Probe's connections in Victim
ephemeral, i.e. Probe's connections are removed after a short time from Victim's
memory. To do so, we use the following options for strongSwan in Victim:

    # In ipsec.conf
    conn probe
        inactivity=5s

    # In strongswan.conf
    charon.inactivity_close_ike=yes
