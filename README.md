# The Deviation Attack

The Deviation Attack is a novel Denial-of-Service attack against IKEv2. Full
details are available in
[paper](https://ieeexplore.ieee.org/abstract/document/8887395) "The Deviation
Attack: A Novel Denial-of-Service Attack Against IKEv2", by T. Ninet, A. Legay,
R. Maillard, L.-M. Traonouez and O. Zendra, published in TrustCom 2019.

The `demo/` directory contains a platform demonstrating the Deviation Attack on
the open-source IKEv2 implementation strongSwan.

The `slides/` directory contains the slides of our TrustCom presentation.

## Recap

![Scenario of the attack](deviation.jpg)

Requirements for the attack:
-   The Initiators authenticate themselves using signature mode and are trusted
    by Victim, or share the same pre-shared key(s) with the Responders as they
    share with Victim
-   The attacker has deviation capabilities between the Initiators and the
    Responders
-   There are enough IKE\_SA\_INIT requests to deviate
-   Each IKE\_SA\_INIT request must come from a different IKEv2 peer

Note that strongswan implements additional checks that are not mandated by the
RFC. As a result strongSwan is only vulnerable if rightid is prefixed with "%".

When the above requirements are satisfied, then an attacker may perform a DoS
attack with a significantly lower throughput than expected from classic flooding
techniques.

In strongswan, when DPD is disabled (which is the default), a DoS is possible
with a throughput 8000 times lower than classic DoS techniques. When DPD is
enabled, by default, a DoS is possible with a throughput 140 times lower than
classic DoS techniques.
